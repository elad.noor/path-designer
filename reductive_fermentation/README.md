# Reductive fermentationparet

This directory contains a script for finding pathways comprised of E. coli enzymes only that produce a fermentation 
product (such as acetate) and use glucose together with another electron source.

To run the pathway search script (optional):
```
python search_pathways.py
```
It will write the pathways to a file called `pathways.tsv`.