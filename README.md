# path-designer
A algorithm for automatic design of metabolic pathways

Requirements:
- CPLEX (from IBM)
- python 3.7+
- numpy
- scipy
- matplotlib
- pandas
- sbtab
- cobra
- optlang
- recommended: escher, pydot, seaborn, jupyterlab

Example:
```
python -m co2_fixation.search_pathways
```
(see results in co2_fixation/pathways.tsv)
