\documentclass{article}
\usepackage[a4paper, margin=2cm]{geometry}

\title{A latent carbon fixation cycle in E. coli mirrors the Calvin Cycle: Supplementary Text}

\usepackage{graphicx}
\usepackage{caption}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{underscore}
\usepackage{siunitx}
\usepackage{pgffor}

\newcommand{\microM}{\si{\micro}M }

\date{\vspace{-5ex}}
\begin{document}
	
	\maketitle
	
	\section{path-designer: an MILP-based algorithm for metabolic pathway design}
	We use a similar approach to the recently published OptMDFpathway method \cite{hadicke-optmdfpathway-2018}. We set up a Mixed Integer Linear Problem (MILP) -based optimization problem which simultaneously looks for solutions that balance an objective reaction (here, 3 CO$_2$ $\rightarrow$ pyruvate), and maximize the Max-min Driving Force (MDF) \cite{noor-pathway-2014} while simultaneously minimizing the number of reactions. In section \ref{sec:alterations}, we lay out the changes we made to the set of reactions in the iML1515 model \cite{monk-iml1515-2017}. In section \ref{sec:milp} we describe how the MILP problem is formulated. Finally, in section \ref{sec:iterations} we describe how we use the MILP framework to find also sub-optimal solutions and cover a large part of the feasible solution space of carbon fixation pathways.
	
	\subsection{Alterations to the iML1515 model}
	\label{sec:alterations}
	We made a few changes to the genome-scale model of \textit{E. coli} \cite{monk-iml1515-2017}:
	\begin{itemize}
		\item Removing all non-cytoplasmic reactions (i.e. exchange or transport reactions), except for exchange reactions of inorganic metabolites: protons, water, orthophosphate, ammonium, and oxygen.
		\item Removing all boundary reactions (i.e. sink reactions needed to allow certain co-factors to leave the system).
		\item Adding co-factor regenerating reactions: ADP $\rightarrow$ ATP, NADP$^+$ $\rightarrow$ NADPH, and NAD$^+$ $\rightarrow$ NADH.
		\item Replacing all flavoredoxins and thioredoxins with NADP $^\dagger$.
		\item Removing the Formate-tetrahydrofolate ligase reaction (FTHFLi) $^\ddagger$.
		\item Add the objective reaction (OBJ): 3 CO$_2$ $\rightarrow$ pyruvate.
		\item Setting the bounds (the range of possible fluxes) of all remaining reactions to be between -10 and 10.
	\end{itemize}
	
	$\dagger$ We replaced all flavoredoxins and thioredoxins with NADP, since we do not have a good estimate of their reduction potential, and therefore the MDF for pathways using them was artificially high. We can assume that the electrons used for reducing CO$_2$ in the carbon fixation cycle ultimately have to pass through NADP, and therefore a simple solution was to replace the electron donor with NADPH. This way, we could keep the flavoredoxins/thioredoxins-dependent reactions in the model while having a more realistic estimate of their thermodynamics.
	
	$\ddagger$ We found that the reaction formate-tetrahydrofolate ligase (FTHFLi) appears in some of the solutions although the gene associated with this reaction is unknown. FTHFLi was thus excluded from our model altogether. Notably, removing this reaction does not significantly affect the space of solutions, because it can be easily replaced by GAR transformylase-T (GART) and the reverse reaction of Phosphoribosylglycinamide formyltransferase (GARFT).
	
	\subsection{Mixed Integer Linear Problem}
	\label{sec:milp}
	\begin{eqnarray}
	\underset{B, \mathbf{v}, \mathbf{z}, \mathbf{x}}{\text{maximize}} & B - \sum_i {z_i} &\label{eq:maximize}\\
	\text{such that}&&\nonumber\\
	& \mathbf{S} \mathbf{v} &= \mathbf{0} \\
	& v_{\text{OBJ}} &= -1 \label{eq:objective}\\
	& \mathbf{v} - \beta \mathbf{z} &\leq 0 \label{eq:indicators}\\
	& B &\leq -\mathbf{g^\circ} ~-~ \mathbf{S}^\top \mathbf{x} ~+~ M(1-\mathbf{z}) \label{eq:driving-force}\\
	& \mathbf{z} &\in \{0, 1\}^n \\
	\mathbf{0} &\leq ~\mathbf{v} &\leq \beta \\
	\ln(\mathbf{C}_\text{min}) &\leq ~\mathbf{x} &\leq \ln(\mathbf{C}_\text{max})\\
	& 0 & \leq B 
	\end{eqnarray}
	where the vector $\mathbf{v}$ contains the relative reaction rates, $\mathbf{z}$ are the Boolean reaction indicators, $\mathbf{x}$ are the log-scaled metabolite concentrations, and $\mathbf{g^\circ}$ is a vector of all the reactions' standard Gibbs free energies in units of $RT$, i.e. $\forall i~g_i^\circ \equiv \Delta_r G'^\circ_i / RT$. $\beta$ is a parameter that limits the maximal rate for each single reaction in the pathway (relative to the objective reaction, i.e. 3 CO$_2$ $\rightarrow$ pyruvate), and was set arbitrarily to 10. The rate of the objective reaction ($v_{\text{OBJ}}$) is set to be exactly -1 (Equation \ref{eq:objective}). This ensures that any pathway solution would exactly balance it, i.e. the overall reaction in the pathway would be 3 CO$_2$ $\rightarrow$ pyruvate. $M$ is a parameter which has a large value, much higher than any of the values in $\mathbf{g^\circ}$. The lower and upper bounds on the concentrations of most metabolites were set to 1 $\mu M$ and 10 mM. Only 14 central metabolites and co-factors were confined to more specific ranges based on physiological data (see Table X1).
	
	\begin{table}
	\centering
	\begin{tabular}{l|l|c}
		Compound & BiGG identifier & Concentration range\\
		\hline
		ATP & atp_c & 5 mM\\
		ADP & adp_c & 0.5 mM -- 2.5 mM\\
		AMP & amp_c & 0.5 mM -- 2.5 mM\\
		NAD$^+$ & nad_c & 1 mM\\
		NADH & nadh_c & 10 \microM -- 100 \microM\\
		NADP$^+$ & nadp_c & 10 \microM\\
		NADPH & nadph_c & 10 \microM -- 100 \microM\\
		O$_2$ & o2_c & 273 \microM\\
		CO$_2$ & co2_c & 6.3 mM\\
		CoA & coa_c & 1 mM -- 5 mM\\
		orthophosphate & pi_c & 1 mM -- 10 mM\\
		pyrophosphate & ppi_c & 0.5 mM -- 1.5 mM\\
		ammonia & nh4_c & 1 mM -- 10 mM\\
		alphaketoglutarate & akg_c & 0.5 mM -- 5 mM\\
		glutamate & glu__L_c & 30 mM -- 150 mM
	\end{tabular}
	\caption*{\textbf{Table X1}: The allowed concentration ranges for metabolites in the model. All metabolites that do not appear in this table, were constrained by the default ranges of 1 \microM to 10 mM.}
	\label{tab:concentrations}
	\end{table}
	
	Note that as a pre-processing step, we split all reactions to a forward and backward reaction and therefore all (uni-directional) rates must be positive. Equation \ref{eq:indicators} ensures that a reaction indicator ($z_i$) can be equal to 0, only if the rate $v_i$ is 0. We don't need to care about $z_i$ being equal to 1 even if a reaction is not active, since the optimization goal (which maximizes the sum of all indicators) will prevent that.
	
	Equation \ref{eq:driving-force} ensures that every active reaction has a positive driving force (which is given by $-g^\circ_i - \sum_j S_{ij}x_j$). If $z_i = 1$, the driving force must be larger than $B$, which is a positive number that represents a margin. We add $B$ to the optimization function, in order to maximize this margin. This approach is based on the Max-min Driving Force \cite{noor-pathway-2014} and aims to prioritize pathways that can be operated as far from equilibrium as possible. The method for using Max-min Driving Force optimization for finding feasible pathways in the genome-scale \textit{E. coli} model was introduced by Hädicke et al. \cite{hadicke-optmdfpathway-2018}, and denoted OptMDFpathway.
	
	Our MILP objective function (Equation \ref{eq:maximize}) is the margin ($B$) minus the sum of all indicators (which is equal to the number of reactions in the pathway). Maximizing this function will simultaneously maximize the MDF and minimize the pathway length. Importantly, when combining two optimization functions, the relative weight given to each one is very important. Since the MDF is given in units of $RT$, and the pathway length is an integer, using equal weights is an arbitrary choice. For example, giving a much higher weight to the MDF (by changing the units, or multiplying it by a large pre-factor) would likely change the MILP solution. In this work, we wanted to avoid tuning the relative optimization weights. Instead, we iterate the space of sub-optimal solutions and try to identify pathways that are Pareto-optimal (i.e., no other solution outperforms them in both MDF and length). This procedure is explained in detail in the next section.
	
	
	\subsection{Iterating the space of solutions}
	\label{sec:iterations}
	In order to cover the space of thermodynamically feasible solutions (i.e. pathways with MDF $>$ 0), we iteratively use integer-cuts to eliminate all previous solutions and find the next optimal one \cite{pharkya-optstrain-2004}. Formally, if $P_0, \ldots, P_m$ are the set of solutions already discovered by our algorithm (where $P_j \subset \{0, \ldots, n\}$) then the added constraints will be:
	\begin{eqnarray}
	\forall j ~~ \sum_{i \in P_j} z_i < |P_j|
	\end{eqnarray}
	where $|P_j|$ is the size of the pathway (i.e. the number of reactions). Each one of these constraints eliminates $P_j$ and any pathway which is a superset of $P_j$ from the solution space.
	
	Using IBM's CPLEX solver, we could recover only $\sim$100 solutions per day, on an Intel Core i7-4770S CPU (with 8 cores). However, when running the iterative search for about 3 days, we noticed that even though the solutions were different by at least one reaction, the overlap between them was quite large and running the search exhaustively would take a significant amount of time. Therefore, in order to increase the diversity of the solutions and shortening the run-time, we changed the solution elimination process, so that each time all solutions within a radius of 3 reactions would also be eliminated. To achieve that, we subtract 3 from the right-hand side of each constraint:
	\begin{eqnarray}
	\forall j ~~ \sum_{i \in P_j} z_i < |P_j| - 3\,.
	\end{eqnarray}
	
	After this modification, the diversity of the pathways within the first 50 was much larger, which we measure by counting which carboxylating enzymes were used in each pathway (see Figure X1 and Table X2). Since we optimize both the MDF and number of reactions in each iteration, it is very unlikely that pathways that are Pareto-optimal would be excluded from the results due to the 3-radius rule. Nevertheless, we verified that the set of Pareto-optimal solutions is not affected by the exclusion radius.
	
	We find that only 2 pathways are Pareto-optimal in terms of MDF and pathway length. The first one (pathway $0$) is the GED cycle, with 17 reactions and an MDF of 3.3 kJ/mol. This is the shortest possible CO$_2$ fixating pathway in \textit{E. coli}. The only other Pareto-optimal pathway, comprising 20 reactions and an MDF of 5.1 kJ/mol, uses the reverse glycine cleavage system as its carboxylating mechanism.

	Graphical depictions of these pathways can be found in section \ref{sec:graphs}.
	
	\begin{figure}[ht!]
		\centering
		\includegraphics[width=\linewidth]{figureX1.pdf}
		\caption*{\textbf{Figure X1}: The MDF versus the number of reactions for all the first 50 solutions. The only two pathways on the Pareto front are 0 and 2. The radius of exclusion around each solution is 3 reactions. The full list of pathways can be found in in section \ref{sec:graphs} or on the
			\href{https://gitlab.com/elad.noor/path-designer}{GitLab} repository. The carboxylating enzymes we found in our pathways are: PPCK (phosphoenolpyruvate carboxykinase), GND (phosphogluconate dehydrogenase), PPC (phosphoenolpyruvate carboxylase), GLYCL (glycine cleavage system), ME1 (malic enyzme NAD-dependent). The Pareto front is marked by a yellow line.}
		\label{fig:pareto}
	\end{figure}

	\begin{table}
	\centering
	\begin{tabular}{c|c|c|l}
Pathway & MDF [kJ/mol] & No. reactions & Carboxylating reactions \\
\hline
0 (GED) & 3.29 & 17.0 & GND\\
1 & 2.93 & 19.0 & ME1 + GND\\
2 & 5.13 & 20.0 & GLYCL\\
3 & 0.17 & 18.0 & GND\\
4 & 2.19 & 19.0 & GND\\
5 & 1.02 & 19.0 & GND\\
6 & 4.27 & 21.0 & GLYCL\\
7 & 0.15 & 20.0 & ME1 + GND\\
8 & 0.84 & 21.0 & GND\\
9 & 5.13 & 23.0 & GLYCL\\
10 & 0.15 & 21.0 & GND\\
11 & 1.11 & 22.0 & ME1 + GND\\
12 & 0.83 & 22.0 & GND\\
13 & 3.12 & 23.0 & ME1 + GLYCL\\
14 & 3.12 & 23.0 & GLYCL + PPC\\
15 & 0.55 & 22.0 & GND\\
16 & 2.87 & 23.0 & GLYCL\\
17 & 5.13 & 24.0 & GLYCL\\
18 & 0.17 & 22.0 & ME1 + GND\\
19 & 2.19 & 23.0 & ME1 + GND\\
20 & 2.19 & 23.0 & ME1 + GND\\
21 & 0.39 & 23.0 & GND\\
22 & 5.13 & 25.0 & GLYCL\\
23 & 4.94 & 25.0 & GLYCL\\
24 & 1.87 & 24.0 & GLYCL\\
25 & 4.27 & 25.0 & GLYCL\\
26 & 1.61 & 24.0 & ME1 + GND\\
27 & 1.53 & 24.0 & GND\\
28 & 1.43 & 24.0 & GND\\
29 & 1.32 & 24.0 & ME1\\
30 & 0.9 & 24.0 & GND\\
31 & 3.13 & 25.0 & PPCK + GLYCL\\
32 & 0.55 & 24.0 & GND\\
33 & 0.44 & 24.0 & GND\\
34 & 0.44 & 24.0 & GND\\
35 & 0.33 & 24.0 & GND\\
36 & 2.64 & 25.0 & PPCK + GLYCL\\
37 & 2.64 & 25.0 & ME1 + GLYCL\\
38 & 0.07 & 24.0 & GLYCL + PPC\\
39 & 0.03 & 24.0 & GND\\
40 & 1.53 & 25.0 & ME1 + GND\\
41 & 3.92 & 26.0 & ME1 + GLYCL\\
42 & 1.32 & 25.0 & ME1\\
43 & 1.32 & 25.0 & ME1\\
44 & 0.95 & 25.0 & GND\\
45 & 0.89 & 25.0 & GND\\
46 & 0.83 & 25.0 & ME1 + GND\\
47 & 0.83 & 25.0 & GND\\
48 & 3.17 & 26.0 & GLYCL\\
49 & 3.12 & 26.0 & PPCK\\
	\end{tabular}
	\caption*{\textbf{Table X2}: The Max-min Driving Force and number of reactions of all 50 top pathways. A graph depiction of these pathways can be found in section \ref{sec:graphs}.}
	\label{tab:concentrations}
	\end{table}
	
	\subsection{Implementation}
	The source code and all input and output files can be found on \href{https://gitlab.com/elad.noor/path-designer/tree/master/co2_fixation}{GitLab} under an open-source license (MIT). Specifically, a Jupyter Notebook which was used to create Figure X1 can be found \href{https://gitlab.com/elad.noor/path-designer/blob/master/co2_fixation/make_supplementary_figures.ipynb}{here}.

\bibliographystyle{plain}		
\bibliography{supplementary}

\clearpage

\subsection{Graphical depictions of the top 50 CO$_2$ fixation pathways}
\label{sec:graphs}
\begin{center}
Pathway 0 (the GED cycle) \\
\includegraphics[height=24cm,width=\textwidth,keepaspectratio]{gdot/pathway0.pdf}
\end{center}
\clearpage

\foreach \n in {1,...,49}{
\begin{center}
	Pathway \n \\
	\includegraphics[height=24cm,width=\textwidth,keepaspectratio]{gdot/pathway\n.pdf}
\end{center}
\clearpage
}

	
\end{document}
