# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import logging
import os
import sys

import pandas as pd
from cobra.io import read_sbml_model

from src import Q_, REC_PATH, ComponentContribution
from src.path import Path

script_path = os.path.abspath(os.path.dirname(__file__))
sys.path.append(os.path.join(script_path, f".."))


###############################################################################

parser = argparse.ArgumentParser(description='CO2 fixation pathway designer')
parser.add_argument(
    '--iter', type=int,
    help='the maximum number of solutions to show.',
    default=50
)
parser.add_argument(
    '--debug', action="store_true",
    help='flag for turning on the debug mode.'
)
args = parser.parse_args()
if args.debug:
    logging.getLogger().setLevel(logging.DEBUG)
else:
    logging.getLogger().setLevel(logging.INFO)

###############################################################################

cobra_model = read_sbml_model(os.path.join(REC_PATH, "iML1515.xml.gz"))

################################################################################
# replace *redoxin (flavoredoxin and thioredoxin) with NADP(H)
thioredoxin = cobra_model.metabolites.trdrd_c
for r in thioredoxin.reactions:
    if r.get_coefficient(thioredoxin) == -1:
        r += cobra_model.reactions.TRDR
    elif r.get_coefficient(thioredoxin) == 1:
        r -= cobra_model.reactions.TRDR

flavoredoxin = cobra_model.metabolites.flxr_c
for r in flavoredoxin.reactions:
    if r.get_coefficient(flavoredoxin) == -2:
        r += cobra_model.reactions.FLDR2
    elif r.get_coefficient(flavoredoxin) == 2:
        r -= cobra_model.reactions.FLDR2

# first, remove the electron transfer reactions from NADPH to *redoxin
cobra_model.reactions.FLDR2.remove_from_model()
cobra_model.reactions.TRDR.remove_from_model()

################################################################################
# formate-THF ligase was mistakenly included in iML1515
cobra_model.reactions.FTHFLi.remove_from_model()
################################################################################

PATH_OBJECTIVE = {"co2_c": -3, "pyr_c": 1}
comp_contrib = ComponentContribution()
comp_contrib.p_h = Q_(7.5)
comp_contrib.ionic_strength = Q_("0.3 M")
CONC_BOUNDS_FNAME = os.path.join(REC_PATH, "co2_to_pyr_concentration.csv")
conc_bounds_df = pd.read_csv(CONC_BOUNDS_FNAME)

# make all the reactions in the model reversible, except for all reactions
# that are boundary (e.g. dummy reactions), non-cytoplasmic, or in the list
# of KOs.
for rxn in cobra_model.reactions:
    if rxn.boundary or (rxn.compartments != {'c'}):
        rxn.remove_from_model()
    else:
        rxn.bounds = (-10, 10)

###############################################################################

logging.info(f"Looking for pathways from CO2 to pyruvate in "
             f"the COBRA model '{cobra_model.id}'. "
             f"Stopping after {args.iter} solutions.")

cf = Path(cobra_model, conc_bounds_df, comp_contrib)
cf.set_pathway_objective(PATH_OBJECTIVE)

cf.add_reaction(f"__regenerate_NADPH__",
                name=f"regenerate NADPH from NADP+",
                metabolites_to_add={"nadp_c": -1, "nadph_c": 1},
                lower_bound=-10,
                upper_bound=10,
                objective_coefficient=0.0)

cf.add_reaction(f"__regenerate_NADH__",
                name=f"regenerate NADH from NAD+",
                metabolites_to_add={"nad_c": -1, "nadh_c": 1},
                lower_bound=-10,
                upper_bound=10,
                objective_coefficient=0.0)

cf.add_reaction("__regenerate_ATP__",
                name="regenerate ATP from ADP",
                metabolites_to_add={"adp_c": -1, "atp_c": 1, "h2o_c": 1,
                                    "pi_c": -1},
                lower_bound=-10,
                upper_bound=10,
                objective_coefficient=0.0)

cf.remove_metabolites(["h_c", "h_e", "h_p"])
for free_met in ["h2o_c", "pi_c", "nh4_c", "o2_c"]:
    cf.add_reaction(id=f"__regenerate_{free_met}__",
                    name=f"regenerate inorganic compound {free_met}",
                    metabolites_to_add={free_met: 1},
                    lower_bound=-10,
                    upper_bound=10,
                    objective_coefficient=0.0)

result_path = os.path.join(script_path, "pathways")
solutions = cf.search(result_path,
                      max_iterations=args.iter,
                      write_lp=False,
                      write_full_solution=False,
                      output_format="none",
                      radius=3)

logging.info(f"Found {len(solutions)} solutions, and wrote them to "
             f"{result_path}")
