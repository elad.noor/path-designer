# Awakening a latent carbon fixation cycle in E. coli

## Pathway search algorithm:

This directory contains a script and a Jupyter Notebook that together
recreate the results and figure X1 for the supplementary text.

To run the pathway search script (optional):
```
python -m co2_fixation.search_pathways
```
It will write the pathways to a file called `co2_fixation/pathways.tsv`. If you skip this
step, you can use the result file that already exists with the 50 top
pathways.

To recreate the figure, open the `co2_fixation/make_supplementary_figures.ipynb` Notebook in
Jupyter, and run all of the cells in sequence.

## Flux Balance Analysis of GED fermentation yields

In `co2_fixation/FBA`, you'll find another Jupyter Notebook which performs runs FBA to calculate
fermentation yields for the GED pathway compared to wild-type E. coli.
