# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import uuid
from typing import Dict, Iterable, Set, TextIO, Tuple, Union

import cobra
import numpy as np
import pandas as pd
import pydot
import seaborn as sns
from escher import Builder
from matplotlib.colors import rgb2hex
from sbtab.SBtab import SBtabDocument, SBtabTable

from . import Q_


class Solution(object):
    """Contained class for path solutions."""

    def __init__(
        self,
        solution_id: int,
        cobra_model: cobra.Model,
        cofactor_set: Set[cobra.Metabolite],
        solution_df: pd.DataFrame,
        mdf: Q_,
    ):
        """
        :param solution_id: a unique index used to identify solutions
        :param cobra_model: the cobra Model used to generate this solution
        :param cofactor_set: the set of cofactors (for visualizing the path)
        :param solution_df: a DataFrame with all the variable and constraint
        data
        :param mdf: the Max-min Driving Force
        """
        self.solution_id = solution_id
        self.cobra_model = cobra_model
        self.cofactor_set = cofactor_set
        self.df = solution_df

        self._mdf = mdf
        assert mdf.check("[energy]/[substance]"), f"MDF units are wrong: {mdf}"
        self._mdf.ito("kJ/mol")

        # construct a specific DataFrame for all the varaibles, and split the
        # name to the 3 parts so that we can use the BiGG IDs in the model later
        self.var_df = solution_df[solution_df.lp_type == "variable"].copy()
        tmp = self.var_df.name.str.split("@", n=3, expand=True)
        self.var_df["bigg_id_type"] = tmp[0]
        self.var_df["type"] = tmp[1]
        self.var_df["bigg_id"] = tmp[2]

        # for reactions, we need to map the 'split' IDs back to the original
        # BiGG identifiers (i.e. reversible reactions)
        flux_df = self.var_df[
            (self.var_df.type == "flux")
            & (self.var_df.bigg_id.str[0:2] != "__")
            & (self.var_df.primal.abs() > 1e-3)
        ].copy()
        tmp = flux_df.bigg_id.str.rsplit("_", n=1, expand=True)
        flux_df["reaction_id"] = tmp[0]
        flux_df["direction"] = tmp[1]
        flux_df.loc[flux_df.direction == "R", "primal"] *= -1.0
        flux_df.drop(["direction", "bigg_id", "bigg_id_type"], axis=1)
        flux_df = flux_df.groupby(["reaction_id", "type"]).sum().reset_index()
        flux_df["reaction"] = flux_df.reaction_id.apply(
            self.cobra_model.reactions.get_by_id
        )
        flux_df["formula"] = flux_df.reaction.apply(lambda r: r.build_reaction_string())

        self.flux_df = flux_df.round(3)

    @property
    def total_flux(self) -> float:
        """The sum of all fluxes"""
        return self.flux_df.primal.abs().sum()

    @property
    def n_reactions(self) -> int:
        """The number of distinct reactions."""
        return self.flux_df.shape[0]

    @property
    def mdf(self) -> Q_:
        """The MDF values"""
        return self._mdf

    @property
    def active_indicators(self) -> Iterable[str]:
        return self.var_df[
            (self.var_df.type == "gamma") & (self.var_df.primal == 1)
        ].name

    def write_to_file(self, handle: TextIO) -> None:
        """Write the solution to a KEGG-style file"""

        handle.write(f"ENTRY       M-PATHWAY_{self.solution_id:03d}\n")
        handle.write(f"SKIP        FALSE\n")
        handle.write(f"NAME        M-PATHWAY_{self.solution_id:03d}\n")
        handle.write(f"TYPE        MARGIN\n")
        handle.write(f"CONDITIONS  pH=7.0,I=0.0,T=300\n")
        handle.write(f"C_MID       0.0001\n")

        first = True
        for row in self.flux_df.itertuples():
            if first:
                prefix = "REACTION"
                first = False
            else:
                prefix = ""
            handle.write(f"{prefix:12s}{row.formula} (x{row.primal})\n")

        handle.write("///\n")
        handle.flush()

    def get_stats_df(self) -> pd.DataFrame:
        """Return a DataFrame with all the statistics of this solution."""
        mdf_unitless = np.round((self.mdf / Q_("kJ/mol")).magnitude, 2)
        stats_df = pd.DataFrame(
            columns=["ID", "QuantityType", "Unit", "Value"],
            data=[
                ("mdf", "max-min driving force", "kJ/mol", mdf_unitless),
                ("nr", "number of reactions", "dimensionless", self.n_reactions),
                ("sum_flux", "sum of fluxes", "dimensionless", self.total_flux),
            ],
        )
        return stats_df

    def write_to_sbtab(self, sbtabdoc: SBtabDocument) -> None:
        """Write the reaction solution to an SBtabTable."""
        reaction_sbtab = SBtabTable.from_data_frame(
            self.flux_df[["reaction_id", "type", "primal", "formula"]],
            table_id=f"Pathway_{self.solution_id:03d}_reactions",
            table_type="Reaction",
        )
        sbtabdoc.add_sbtab(reaction_sbtab)

        stats_sbtab = SBtabTable.from_data_frame(
            self.get_stats_df(),
            table_id=f"Pathway_{self.solution_id:03d}_stats",
            table_type="Quantity",
        )
        sbtabdoc.add_sbtab(stats_sbtab)

    def to_metabolite_sbtab(self) -> Union[SBtabTable, None]:
        """Write the metabolite solution to an SBtabTable."""
        active_met_ids = set()
        for rxn in self.flux_df.reaction:
            active_met_ids.update([met.id for met in rxn.metabolites])

        metabolite_df = self.var_df[
            (self.var_df.bigg_id_type == "M")
            & (self.var_df.bigg_id.isin(active_met_ids))
        ]

        if metabolite_df.shape[0] == 0:
            return None

        return SBtabTable.from_data_frame(
            metabolite_df.round(3),
            table_id=f"Pathway_{self.solution_id:03d}_metabolites",
            table_type="Compound",
        )

    def create_metabolite_node(self, met: cobra.Metabolite) -> pydot.Node:
        """Create a graphviz Node for a Metabolite.

        :param met: A Metabolite
        :param cofactor_set: the set of all co-factor metabolites
        :return: a pydot.Node object
        """
        node = pydot.Node(name=str(uuid.uuid1()))
        node.set("label", f'"{met.id}"')
        node.set("tooltip", met.name)
        node.set("fontsize", "8")
        node.set("fontname", "verdana")
        node.set("style", "filled")

        url = (
            f'"http://bigg.ucsd.edu/universal/metabolites/'
            f"{met.id.rsplit('_', 1)[0]}\""
        )
        if met.id[0:2] == "__":
            node.set("shape", "tripleoctagon")
            node.set("fontcolor", "darkgreen")
            node.set("fillcolor", "white")
        elif met in self.cofactor_set:
            node.set("shape", "plaintext")
            node.set("URL", url)
            node.set("fontcolor", "dodgerblue")  # color for cofactors
            node.set("fillcolor", "white")
        else:
            node.set("shape", "octagon")
            node.set("URL", url)
            node.set("fontcolor", "white")  # color for non-cofcators
            node.set("fillcolor", "dodgerblue")
        return node

    @staticmethod
    def create_reaction_node(rxn: cobra.Reaction) -> pydot.Node:
        """Create a graphviz node for a Reaction.

        :param rxn: a Reaction
        :return: a pydot.Node object
        """
        node = pydot.Node(name=str(uuid.uuid1()))
        node.set("label", f'"{rxn.id}"')
        node.set("tooltip", rxn.id)
        node.set("URL", f'"http://bigg.ucsd.edu/universal/reactions/{rxn.id}"')
        node.set("shape", "oval")
        node.set("style", "filled")
        node.set("fontcolor", "darkgreen")
        node.set("fillcolor", "white")
        node.set("fontsize", "5")
        node.set("fontname", "verdana")
        return node

    @staticmethod
    def create_edge(
        m_node: pydot.Node, r_node: pydot.Node, weight: float = 1.0, flux: float = 1.0
    ) -> pydot.Edge:
        """Create a graphviz edge between a metabolite and a reaction.

        :param m_node: A Metabolite Node
        :param r_node: A Reaction Mode
        :param weight: the graphviz weight for this edge
        :param flux: the flux going through this reaction
        :return: A pydot.Edge object
        """

        if flux < 0:
            edge = pydot.Edge(m_node, r_node)
        else:
            edge = pydot.Edge(r_node, m_node)

        if abs(abs(flux) - 1.0) > 1e-3:
            edge.set("label", f'"{abs(flux):.3g}"')

        edge.set("arrowhead", "open")
        edge.set("arrowtail", "none")
        edge.set("color", "cadetblue")  # edge line color
        edge.set("fontcolor", "indigo")  # edge label color
        edge.set("fontname", "verdana")
        edge.set("fontsize", "8")
        edge.set("weight", weight)
        return edge

    def to_graph(
        self
    ) -> Tuple[
        pydot.Dot, Dict[cobra.Reaction, pydot.Node], Dict[cobra.Reaction, pydot.Node]
    ]:
        """Draw the solution using graphviz.

        :return: (g_dot, r_nodes, m_nodes) - g_dot is the Dot object for
        drawing the pathway, r_nodes and m_nodes are dictionaries containing
        all the nodes in the bipartite graph (for easy access later)
        """
        g_dot = pydot.Dot(newrank=True)

        r_nodes: Dict[cobra.Reaction, pydot.Node] = {}  # dict of reaction nodes
        m_nodes: Dict[cobra.Reaction, pydot.Node] = {}  # dict of metabolite

        for row in self.flux_df.itertuples():
            node = Solution.create_reaction_node(row.reaction)
            r_nodes[row.reaction_id] = node
            g_dot.add_node(node)

            for met in row.reaction.metabolites:
                if met in self.cofactor_set:
                    # make a special node of this co-factor for each reaction
                    # that uses it
                    m_node = self.create_metabolite_node(met)
                    g_dot.add_node(m_node)
                elif met not in m_nodes:
                    # This is a normal metabolite, create only one node
                    # for it
                    m_node = self.create_metabolite_node(met)
                    g_dot.add_node(m_node)
                    m_nodes[met] = m_node
                else:
                    m_node = m_nodes[met]

                edge = Solution.create_edge(
                    m_node,
                    r_nodes[row.reaction_id],
                    weight=1.0 if met in self.cofactor_set else 5.0,
                    flux=row.reaction.get_coefficient(met) * row.primal,
                )
                g_dot.add_edge(edge)
        return g_dot, r_nodes, m_nodes

    def to_escher(self, map_name=None) -> Builder:
        """Use Escher to draw the flux solution as a network."""
        if map_name is None:
            if self.cobra_model.id == "iJO1366":
                map_name = "iJO1366.Central metabolism"
            elif self.cobra_model.id == "e_coli_core":
                map_name = "e_coli_core.Core metabolism"
            else:
                raise KeyError(
                    f"This COBRA model ({self.cobra_model.id}) "
                    "isn't in our list, please provide the map_name "
                    "for Escher."
                )

        reaction_scale = []
        for f, s, c in zip(
            ["min", "Q1", "median", "Q3", "max"],
            [0, 10, 15, 20, 25],
            sns.color_palette("viridis", n_colors=5),
        ):
            reaction_scale.append({"type": f, "size": s, "color": f"{rgb2hex(c)}"})

        flux_dict = self.flux_df.set_index("reaction_id").primal.to_dict()
        return Builder(
            map_name=map_name,
            model=self.cobra_model,
            reaction_data=flux_dict,
            reaction_styles=["color", "size", "abs", "text"],
            reaction_scale=reaction_scale,
            reaction_no_data_color="#eeeeee",
        )
