# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import logging

import cobra.test

from src import RES_PATH
from src.path import Path

###############################################################################

parser = argparse.ArgumentParser(description="CO2 fixation pathway designer")
parser.add_argument(
    "--iter", type=int, help="the maximum number of solutions to show.", default=1000
)
parser.add_argument(
    "--debug", action="store_true", help="flag for turning on the debug mode."
)
parser.add_argument(
    "--anaerobic", action="store_true", help="flag for turning on the anaerobic mode."
)
args = parser.parse_args()
if args.debug:
    logging.getLogger().setLevel(logging.DEBUG)
else:
    logging.getLogger().setLevel(logging.INFO)

###############################################################################

cobra_model = cobra.test.create_test_model("ecoli")

PATH_OBJECTIVE = {"co2_c": -3, "pyr_c": 1}
FREE_METABOLITES = ["h2o_c", "pi_c", "nh4_c", "o2_c"]
REDOX_PAIRS = [
    ("nad_c", "nadh_c"),
    ("nadp_c", "nadph_c"),
    ("q8_c", "q8h2_c"),
    ("mqn8_c", "mql8_c"),
    ("trdox_c", "trdrd_c"),
    ("flxso_c", "flxr_c"),
    ("fad_c", "fadh2_c"),
]

cobra_model.metabolites.acon__C_c.id = "acon_C_c"  # fix bug in model
PROTONS = ["h_c", "h_e", "h_p"]

# remove non-cytoplasmic reactions
# for rxn in cobra_model.reactions:
#    if rxn.compartments != {'c'}:
#        rxn.knock_out()

###############################################################################

logging.info(
    f"Looking for pathways from CO2 to pyruvate in "
    f"the COBRA model '{cobra_model.id}'. "
    f"Stopping after {args.iter} solutions."
)

cf = Path(cobra_model)
cf.set_pathway_objective(PATH_OBJECTIVE)

for ox, red in REDOX_PAIRS:
    cf.add_reaction(
        f"__regenerate_{red}__",
        {ox: -1, red: 1},
        lower_bound=-1000,
        upper_bound=1000,
        objective_coefficient=0.0,
    )

cf.add_reaction(
    "__regenerate_atp__",
    {"adp_c": -1, "atp_c": 1, "h2o_c": 1, "pi_c": -1},
    lower_bound=-1000,
    upper_bound=1000,
    objective_coefficient=0.0,
)

cf.remove_metabolites(PROTONS)
for free_met in FREE_METABOLITES:
    cf.add_reaction(
        f"__regenerate_{free_met}__",
        {free_met: 1},
        lower_bound=-1000,
        upper_bound=1000,
        objective_coefficient=0.0,
    )

solutions = cf.find_paths(
    RES_PATH,
    max_iterations=args.iter,
    write_lp=False,
    write_full_solution=False,
    output_format="none",
)
