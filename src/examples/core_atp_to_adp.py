# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import logging

from cobra.test import create_test_model

from src.path import Path

logging.getLogger().setLevel(logging.INFO)

cobra_model = create_test_model("textbook")
for reaction_id in ["ATPM", "ATPS4r", "EX_glc__D_e", "Biomass_Ecoli_core"]:
    cobra_model.reactions.get_by_id(reaction_id).knock_out()

cf = Path(cobra_model)
cf.set_pathway_objective({"atp_c": 1, "adp_c": -1})
cf.currency_metabolites = {"h2o_c", "atp_c", "adp_c", "pi_c"}

# cf.merge_metabolites("NAD(P)", ["nad_c", "nadp_c"])
# cf.merge_metabolites("NAD(P)H", ["nadh_c", "nadph_c"])
# cf.add_reaction("__regenerate_ATP__", {"adp_c": -1, "atp_c": 1})
# cf.add_reaction("__regenerate_NAD__", {"NAD(P)": -1, "NAD(P)H": 1})
cf.remove_metabolites(["h2o_c", "h_c", "h_e"])
for free_met in ["pi_c", "co2_c", "o2_c"]:
    cf.add_reaction(
        f"__regenerate_{free_met}__", {free_met: 1}, lower_bound=-1000, upper_bound=1000
    )

cf.add_cofactors(
    ["adp_c", "atp_c", "nadp_c", "nadph_c", "nad_c", "nadh_c", "co2_c", "o2_c", "pi_c"]
)
cf.find_paths(
    "core_atp_to_adp",
    milp_factor=0,
    max_iterations=1,
    write_lp=True,
    output_format="both",
)
