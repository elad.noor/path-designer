# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import logging
import os

from cobra.test import create_test_model

from src import RES_PATH
from src.path import Path

logging.getLogger().setLevel(logging.DEBUG)

cobra_model = create_test_model("textbook")
result_path = os.path.join(RES_PATH, f"{cobra_model.id}_g6p_to_pyr")

for rxn in cobra_model.reactions:
    if rxn.boundary or (rxn.compartments != {"c"}):
        rxn.remove_from_model()
    else:
        rxn.bounds = (-10, 10)

cf = Path(cobra_model)
cf.set_pathway_objective({"g6p_c": -1, "adp_c": -1, "pyr_c": 2, "atp_c": 1})
# cf.add_currency_metabolites({"h2o_c", "atp_c", "adp_c", "amp_c", "ppi_c",
#                             "pi_c", "co2_c"})

# cf.add_reaction("__regenerate_ATP__", {"adp_c": -1, "atp_c": 1})
cf.add_reaction(
    id="__regenerate_NADH__",
    name="regererate NADH from NAD",
    metabolites_to_add={"nad_c": -1, "nadh_c": 1},
    lower_bound=-10,
    upper_bound=10,
    objective_coefficient=0.0,
)
cf.add_reaction(
    id="__regenerate_NADPH__",
    name="regererate NADPH from NADP",
    metabolites_to_add={"nadp_c": -1, "nadph_c": 1},
    lower_bound=-10,
    upper_bound=10,
    objective_coefficient=0.0,
)

cf.remove_metabolites(["h2o_c", "h_c", "h_e"])
for free_met in ["pi_c", "co2_c", "o2_c"]:
    cf.add_reaction(
        id=f"__regenerate_{free_met}__",
        name=f"regenerate {free_met}",
        metabolites_to_add={free_met: 1},
        lower_bound=-10,
        upper_bound=10,
        objective_coefficient=0.0,
    )

cf.add_cofactors(
    [
        "adp_c",
        "atp_c",
        "amp_c",
        "nad_c",
        "nadp_c",
        "nadh_c",
        "nadph_c",
        "co2_c",
        "o2_c",
        "pi_c",
    ]
)
cf.search(res_path=result_path, max_iterations=4, output_format="both")
