# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""A script for finding all possible pathways from A to B."""
import json
import logging
import re
from typing import Dict, Iterable, List, Set, Tuple, Union

import cobra
import pandas as pd
from optlang import Constraint, Model, Objective, Variable, symbolics
from sbtab.SBtab import SBtabDocument

from . import Q_, Bounds, ComponentContribution
from .solution import Solution
from .util import calculate_standard_dgs


class Path(object):
    """Finds pathways using the Universal stoichiometric matrix."""

    def __init__(
        self,
        cobra_model: cobra.Model,
        bounds_df: pd.DataFrame = None,
        comp_contrib: ComponentContribution = None,
    ):
        """Create a Path for designing pathways based on goal stoichiometry.

        :param cobra_model: a COBRA model, from which to take the list of
        reactions.
        """
        self.cobra_model = cobra_model.copy()
        self.comp_contrib = comp_contrib
        if bounds_df is not None:
            self.bounds, self.name_to_compound = Bounds.from_dataframe(
                bounds_df, self.comp_contrib
            )
        else:
            self.bounds = Bounds.GetDefaultBounds(self.comp_contrib)
            self.name_to_compound = dict()

        self.cofactor_set: Set[cobra.Metabolite] = set()
        self.lp_model = None
        self.__sum_of_fluxes_ub = 100.0
        self.__mdf_lb = Q_("0.0 kJ/mol")
        self._objective_coefficients: Dict[str, float] = dict()
        self._currency_metabolites = set()
        logging.debug("Setting the objective coefficients of all reactions " "to 1")

        for rxn in self.cobra_model.reactions:
            self.set_objective_coefficient(rxn, 1.0)

        if self.comp_contrib is not None:
            self.standard_dg = calculate_standard_dgs(
                self.cobra_model, self.comp_contrib
            )

    def set_objective_coefficient(
        self, rxn: Union[str, cobra.Reaction], coefficient: float
    ) -> None:
        """Set the objective coefficient of a reaction.

        :param rxn: cobra Reaction or Reaction ID
        :param coefficient: the objective coefficient
        """
        assert coefficient >= 0, "Objective coefficients must be positive or 0"
        if type(rxn) == str:
            self._objective_coefficients[rxn] = coefficient
        else:
            self._objective_coefficients[rxn.id] = coefficient

    def get_objective_coefficient(self, rxn: Union[str, cobra.Reaction]) -> float:
        """Get the objective coefficient of a reaction.

        :param rxn: cobra Reaction or Reaction ID
        :return: the objective coefficient
        """
        if type(rxn) == str:
            return self._objective_coefficients.get(rxn, 0.0)
        else:
            return self._objective_coefficients.get(rxn.id, 0.0)

    def add_currency_metabolites(
        self, currency_metabolites: Iterable[Union[str, cobra.Metabolite]]
    ) -> None:
        """Add metabolites to the set of currency metabolites

        :param currency_metabolites: iterable of metabolites to add
        """
        for met in currency_metabolites:
            if type(met) == str:
                self._currency_metabolites.add(met)
            else:
                self._currency_metabolites.add(met.id)

    @property
    def metabolites(self) -> Iterable[cobra.Metabolite]:
        """Return the list of COBRA metabolites in the model."""
        return self.cobra_model.metabolites

    @property
    def reactions(self) -> Iterable[cobra.Reaction]:
        """Return the list of COBRA reactions in the model."""

        # First check for duplicate reactions, i.e. reactions that are
        # identical in terms of stoichiometry
        rxn_hashes = []
        for rxn in self.cobra_model.reactions:
            rxn_hashes.append((rxn, str(rxn.metabolites)))
        df = pd.DataFrame(data=rxn_hashes, columns=["reaction", "hash"])
        unique_df = df.drop_duplicates(subset="hash", keep="first")

        return unique_df.reaction.values

    def get_currency_metabolites(self) -> Set[Union[cobra.Metabolite, str]]:
        """Get the set of currency metabolites (used to define type II
        cycles)."""
        return self.__currency_metabolites

    def set_currency_metabolites(self, x: Set[Union[cobra.Metabolite, str]]):
        """Set the set of currency metabolites (used to define type II
        cycles)."""
        self.__currency_metabolites = x

    @property
    def stoichiometry_df(self) -> pd.DataFrame:
        """Convert the stoichiometric data to a DataFrame.

        :return: a DataFrame containing the sparse stoichiometric matrix.
        """
        stoichiometric_data = []
        for rxn in self.reactions:
            for met in rxn.metabolites:
                stoichiometric_data.append((rxn, met, met.id, rxn.get_coefficient(met)))
        return pd.DataFrame(
            stoichiometric_data,
            columns=["reaction", "metabolite", "metabolite_id", "coefficient"],
        )

    def add_reaction(
        self,
        id: str,
        name: str,
        metabolites_to_add: Dict[Union[cobra.Metabolite, str], float],
        lower_bound: float = -1000.0,
        upper_bound: float = 1000.0,
        objective_coefficient: float = 1.0,
    ):
        """Insert (or override) a reaction to the COBRA model."""
        logging.debug(f"Adding reaction: {id}")

        if self.cobra_model.reactions.has_id(id):
            self.cobra_model.reactions.get_by_id(id).remove_from_model()
        rxn = cobra.Reaction(
            id=id, name=name, lower_bound=lower_bound, upper_bound=upper_bound
        )
        self.cobra_model.add_reaction(rxn)
        rxn.add_metabolites(metabolites_to_add)
        self.set_objective_coefficient(rxn, objective_coefficient)
        return rxn

    def set_pathway_objective(
        self, metabolites_to_add: Dict[Union[cobra.Metabolite, str], float]
    ) -> None:
        """Add a dictionary of the overall reaction required for this path
        search problem."""
        logging.debug("Setting pathway objective")
        self.add_reaction(
            id="__path_objective__",
            name="objective",
            metabolites_to_add=metabolites_to_add,
            lower_bound=-1 - 1e-5,
            upper_bound=-1 + 1e-5,
            objective_coefficient=0.0,
        )

    @property
    def mdf_lb(self) -> Q_:
        """Get the lower bound on the MDF."""
        return self.__mdf_lb

    @mdf_lb.setter
    def mdf_lb(self, lb: Q_) -> None:
        """Set the lower bound for the MDF. The default is 0."""
        self.__mdf_lb = lb

    @property
    def sum_of_fluxes_ub(self) -> float:
        """Get the lower bound on the MDF."""
        return self.__sum_of_fluxes_ub

    @sum_of_fluxes_ub.setter
    def sum_of_fluxes_ub(self, ub: float) -> None:
        """Set the lower bound for the MDF. The default is 0."""
        self.__sum_of_fluxes_ub = ub

    def merge_metabolites(
        self,
        new_metabolite_name: str,
        old_metabolites: Iterable[Union[cobra.Metabolite, str]],
    ):
        """Merge a list of metabolites into one. This helps in deduping
        reactions later."""

        group_met = cobra.Metabolite(id=new_metabolite_name)
        self.cobra_model.add_metabolites(group_met)
        for met in old_metabolites:
            if type(met) == str:
                met = self.cobra_model.metabolites.get_by_id(met)
            for rxn in met.reactions:
                rxn.add_metabolites({group_met: rxn.get_coefficient(met)})
            self.cobra_model.remove_metabolites(met)

    def remove_metabolites(
        self, metabolites: Iterable[Union[cobra.Metabolite, str]]
    ) -> None:
        """Remove metabolites from the model."""
        for met in metabolites:
            if type(met) == str:
                met = self.cobra_model.metabolites.get_by_id(met)
            self.cobra_model.remove_metabolites(met)

    def add_cofactors(self, cofactors: Iterable[Union[cobra.Metabolite, str]]) -> None:
        """Add metabolites to the set of co-factors (only affects the
        pathway visualization)."""
        for c in cofactors:
            if type(c) == str:
                c = self.cobra_model.metabolites.get_by_id(c)
            self.cofactor_set.add(c)

    def _create_min_flux_lp(self) -> None:
        """This is only for debugging purposes."""
        logging.debug(f"Gathering reactions from COBRA model")

        irreversible_reaction = []
        for rxn in self.reactions:
            obj_coeff = self.get_objective_coefficient(rxn)
            # Split this reaction to forward and backward
            if rxn.upper_bound > 0:
                irreversible_reaction.append(
                    (
                        rxn,
                        rxn.id + "_F",
                        1,
                        max(rxn.lower_bound, 0),
                        rxn.upper_bound,
                        obj_coeff,
                    )
                )
            if rxn.lower_bound < 0:
                irreversible_reaction.append(
                    (
                        rxn,
                        rxn.id + "_R",
                        -1,
                        max(-rxn.upper_bound, 0),
                        -rxn.lower_bound,
                        obj_coeff,
                    )
                )

        df = pd.DataFrame(
            data=irreversible_reaction,
            columns=[
                "reaction",
                "reaction_id",
                "direction",
                "lower_bound",
                "upper_bound",
                "objective_coefficient",
            ],
        )

        logging.debug(
            f"Creating flux variables and indicators, for the "
            f"{df.shape[0]} one-way reactions"
        )

        df["flux"] = [
            Variable(
                f"R@flux@{row.reaction_id}",
                lb=row.lower_bound,
                ub=row.upper_bound,
                type="continuous",
            )
            for row in df.itertuples()
        ]

        self.lp_model = Model(name="path-designer")
        logging.debug(
            f"Creating path-finding LP using solver: "
            f"{self.lp_model.interface.__name__}"
        )
        self.lp_model.configuration.verbosity = 0
        self.lp_model.update()

        # only count the irreversible fluxes
        logging.debug(f"Adding mass balance constraints")

        # add stoichiometry information to each irreversible reaction
        stoich_df = df.merge(self.stoichiometry_df, on="reaction")

        # adjust the sign of coefficients to the new "irreversible" directions
        stoich_df.coefficient = stoich_df.coefficient.multiply(stoich_df.direction)

        # add the flux variables to the DataFrame
        stoich_df["flux_times_coeff"] = stoich_df.flux.multiply(stoich_df.coefficient)

        mass_balance_constraints = []
        for met_id, group_df in stoich_df.groupby("metabolite_id"):
            if group_df.shape[0] == 1:
                expr = group_df.flux_times_coeff.iat[0]
            else:
                expr = symbolics.add(group_df.flux_times_coeff)
            mass_balance_constraints.append(
                Constraint(expr, lb=0.0, ub=0.0, name=f"M@mass_balance@{met_id}")
            )

        self.lp_model.add(mass_balance_constraints)
        self.lp_model.update()

        sum_of_fluxes = symbolics.add(df.flux)

        logging.debug(f"Adding minimum sum of all indicators objective")
        self.lp_model.objective = Objective(
            sum_of_fluxes, direction="min", name="sum_of_fluxes"
        )
        self.lp_model.update()

    def create_min_step_lp(self) -> None:
        """Create the LP for finding the path with min number of reactions."""
        logging.debug(f"Gathering reactions from COBRA model")

        irreversible_reaction = []
        for rxn in self.reactions:
            obj_coeff = self.get_objective_coefficient(rxn)
            # Split this reaction to forward and backward
            if rxn.upper_bound > 0:
                irreversible_reaction.append(
                    (
                        rxn,
                        rxn.id + "_F",
                        1,
                        max(rxn.lower_bound, 0),
                        rxn.upper_bound,
                        obj_coeff,
                    )
                )
            if rxn.lower_bound < 0:
                irreversible_reaction.append(
                    (
                        rxn,
                        rxn.id + "_R",
                        -1,
                        max(-rxn.upper_bound, 0),
                        -rxn.lower_bound,
                        obj_coeff,
                    )
                )

        df = pd.DataFrame(
            data=irreversible_reaction,
            columns=[
                "reaction",
                "reaction_id",
                "direction",
                "lower_bound",
                "upper_bound",
                "objective_coefficient",
            ],
        )

        logging.debug(
            f"Creating flux variables and indicators, for the "
            f"{df.shape[0]} one-way reactions"
        )

        df["flux"] = [
            Variable(
                f"R@flux@{row.reaction_id}",
                lb=row.lower_bound,
                ub=row.upper_bound,
                type="continuous",
            )
            for row in df.itertuples()
        ]

        df["gamma"] = [
            Variable(f"R@gamma@{row.reaction_id}", type="binary")
            if row.objective_coefficient > 0
            else None
            for row in df.itertuples()
        ]
        df_obj = df[df.objective_coefficient > 0].copy()

        logging.debug(f"Creating indicator/flux constraints")
        # Make each gamma into a flux indicator
        # (i.e. so that if v_i > 0 then gamma_i must be equal to 1).
        # We use the following constraint:
        #          - M - 1 <= v_i - M * gamma_i <= 0
        df_obj["indicator_constraint"] = [
            Constraint(
                row.flux - row.upper_bound * row.gamma,
                lb=(-row.upper_bound - 1.0),
                ub=0.0,
                name=f"R@constr_gamma@{row.reaction_id}",
            )
            for row in df_obj.itertuples()
        ]

        self.lp_model = Model(name="path-designer")
        logging.debug(
            f"Creating path-finding LP using solver: "
            f"{self.lp_model.interface.__name__}"
        )
        self.lp_model.configuration.verbosity = 0
        self.lp_model.add(df_obj.indicator_constraint)
        self.lp_model.update()

        # only count the irreversible fluxes
        logging.debug(
            f"Adding upper bound for the sum of fluxes: " f"{self.sum_of_fluxes_ub}"
        )
        sum_of_fluxes = symbolics.add(df[df.objective_coefficient > 0].flux)
        self.lp_model.add(
            Constraint(
                sum_of_fluxes, lb=0, ub=self.sum_of_fluxes_ub, name="sum_of_fluxes"
            )
        )

        logging.debug(f"Adding mass balance constraints")

        # add stoichiometry information to each irreversible reaction
        stoich_df = df.merge(self.stoichiometry_df, on="reaction")

        # adjust the sign of coefficients to the new "irreversible" directions
        stoich_df.coefficient = stoich_df.coefficient.multiply(stoich_df.direction)

        # add the flux variables to the DataFrame
        stoich_df["flux_times_coeff"] = stoich_df.flux.multiply(stoich_df.coefficient)

        mass_balance_constraints = []
        for met_id, group_df in stoich_df.groupby("metabolite_id"):
            if group_df.shape[0] == 1:
                expr = group_df.flux_times_coeff.iat[0]
            else:
                expr = symbolics.add(group_df.flux_times_coeff)
            mass_balance_constraints.append(
                Constraint(expr, lb=0.0, ub=0.0, name=f"M@mass_balance@{met_id}")
            )

        self.lp_model.add(mass_balance_constraints)
        self.lp_model.update()

        if self._currency_metabolites:
            self._add_loopless_constraints(stoich_df)

        weighted_sum_of_gammas = symbolics.add(
            df_obj.gamma.multiply(df_obj.objective_coefficient)
        )
        if self.comp_contrib is not None:
            logging.debug(f"Adding OptMDFpathway objective")
            margin = self._add_optmdf_constraints(stoich_df)
            # the new objective is the max MDF, but we also minimize the sum of
            # fluxes as a secondary objective
            self.lp_model.objective = Objective(
                margin - weighted_sum_of_gammas, direction="max", name="mdf"
            )
            self.lp_model.update()
        else:
            logging.debug(f"Adding minimum sum of all indicators objective")
            self.lp_model.objective = Objective(
                weighted_sum_of_gammas, direction="min", name="weighted_sum_of_gammas"
            )
            self.lp_model.update()

    def _add_loopless_constraints(self, stoich_df: pd.DataFrame) -> None:
        """Constraints that remove type II futile cycles.

        In other words, loops that only cycle currency metabolites. To
        achieve this, we create a set of metabolite potential variables and
        add them to the big DataFrame.

        :param stoich_df: a DataFrame containing all variables and the
        stoichiometry
        """
        non_currency_met_ids = set(stoich_df.metabolite_id).difference(
            self._currency_metabolites
        )

        metabolite_potentials = pd.Series(
            {
                met_id: Variable(f"M@loopless@{met_id}", type="continuous")
                for met_id in non_currency_met_ids
            },
            name="M@loopless",
        )
        df_without_currency = stoich_df.join(
            metabolite_potentials, on="metabolite_id", how="inner"
        )

        # Then we calculate the reaction potentials (by multiplying the
        # the metabolite potential vector by S).
        df_without_currency[
            "G_loopless_times_coeff"
        ] = df_without_currency.G_loopless.multiply(df_without_currency.coefficient)

        loopless_constraints = []
        large_energy_value = 1000.0
        driving_force_margin = 1e-1
        logging.debug(f"Adding loop-less rules to remove type II cycles")
        for rxn_id, group_df in df_without_currency.groupby("reaction_id"):
            if rxn_id[0:2] != "__":
                gamma = group_df.gamma.iat[0]
                G_r = symbolics.add(group_df.G_loopless_times_coeff.values)
                # Add the constraints on G_i, i.e. G_i < 0 if gamma_i = 1:
                #          -M <= G_r + (M + epsilon) * gamma_i <= M
                loopless_constraints.append(
                    Constraint(
                        G_r + (large_energy_value + driving_force_margin) * gamma,
                        lb=-large_energy_value,
                        ub=large_energy_value,
                        name=f"R@loopless@{rxn_id}",
                    )
                )
        self.lp_model.add(loopless_constraints)
        self.lp_model.update()

    def _add_optmdf_constraints(self, stoich_df: pd.DataFrame) -> Variable:
        """Add the thermodynamic constraints (TFA)

        :param stoich_df: a DataFrame containing all variables and the
        stoichiometry
        """

        # create the ln concentration variables, and give them the proper
        # bounds
        # create the ln concentration variables, and give them the proper
        # bounds

        logging.debug(f"Defining log-concentration variables")
        all_met_ids = set(stoich_df.metabolite_id)

        met_ids_with_bounds, compounds = zip(*self.name_to_compound.items())
        default_lb = Bounds.conc2ln_conc(self.bounds.GetLowerBound(None))
        default_ub = Bounds.conc2ln_conc(self.bounds.GetUpperBound(None))
        lnC_dict = {
            met_id: Variable(f"M@lnC@{met_id}", lb=default_lb, ub=default_ub)
            for met_id in all_met_ids.difference(met_ids_with_bounds)
        }

        lbs, ubs = self.bounds.GetLnBounds(compounds)
        lnC_dict.update(
            {
                met_id: Variable(f"M@lnC@{met_id}", lb=lb, ub=ub)
                for met_id, lb, ub in zip(met_ids_with_bounds, lbs, ubs)
            }
        )

        df = stoich_df.join(pd.Series(lnC_dict, name="lnC"), on="metabolite_id")

        # Then we calculate the reaction potentials (by multiplying the
        # the metabolite potential vector by S).
        df["lnC_times_coeff"] = df.lnC.multiply(df.coefficient)

        # The precalculated dG'0 values are in units of RT. We also ignore
        # all those that have a high uncertainty.
        # TODO: use the dG'0 uncertainty matrix in the MDF formulation
        thermo_df = self.standard_dg[
            ~pd.isnull(self.standard_dg.standard_dg_prime)
            & (self.standard_dg.standard_dg_prime_error < 1000)
        ]

        fwd_df = thermo_df.copy()
        fwd_df["reaction_id"] = thermo_df.bigg_id + "_F"
        fwd_df.set_index("reaction_id", inplace=True)

        rev_df = thermo_df.copy()
        rev_df["reaction_id"] = thermo_df.bigg_id + "_R"
        rev_df.set_index("reaction_id", inplace=True)
        rev_df["standard_dg_prime"] *= -1.0

        thermo_df = pd.concat([fwd_df, rev_df])
        df = df.join(thermo_df, on="reaction_id", how="inner")

        large_energy_value = 1000.0

        # the margin is in units of RT
        mdf_lb_over_rt = float((self.mdf_lb / self.comp_contrib.RT).magnitude)
        margin = Variable(
            "MDF_margin", lb=mdf_lb_over_rt, ub=large_energy_value, type="continuous"
        )
        logging.debug(f"Adding thermodynamic constraints")
        self.lp_model.add([margin])
        self.lp_model.update()
        for irrev_rxn_id, group_df in df.groupby("reaction_id"):
            standard_dg_prime = group_df.standard_dg_prime.iat[0]
            gamma = group_df.gamma.iat[0]
            dg_prime = float(standard_dg_prime) + (
                symbolics.add(group_df.lnC_times_coeff.values)
            )

            # Add the constraints on dG', i.e.:
            #        margin <= -dG'/RT + M * (1-gamma)
            #
            # which translates to:
            #        margin + M*gamma + dG'/RT <= M
            cnstr = Constraint(
                margin + large_energy_value * gamma + dg_prime,
                lb=-large_energy_value,
                ub=large_energy_value,
                name=f"R@thermodynamic@{irrev_rxn_id}",
            )
            self.lp_model.add([cnstr])
        self.lp_model.update()

        return margin

    def exclude_solution(self, solution: Solution, radius: int = 0) -> None:
        """Exclude a solution from the LP.

        :param solution: the solution to exclude
        :param radius: the radius (distance in number of reactions) around
        this solution that should be excluded. Default is 0, which means we
        exclude only the solution itself.
        :return:
        """

        # for each previous solution, add constraints on the indicator
        # variables, so that that solution will not repeat (i.e. the sum
        # over the previous reaction set must be less than the size of
        # that set).
        logging.debug("Adding constraints: exclude previous solutions")

        sum_of_objective_coefficients = 0.0
        weighted_sum_of_gammas = []
        for var_name in solution.active_indicators:
            gamma = self.lp_model.variables[var_name]
            tokens = re.findall(r"R@gamma@(\w+)_([FR])", var_name)
            assert len(tokens) == 1, var_name
            reaction_id, direction = tokens[0]
            objective_coefficient = self.get_objective_coefficient(reaction_id)
            if objective_coefficient > 0:
                weighted_sum_of_gammas.append(gamma * objective_coefficient)
                sum_of_objective_coefficients += objective_coefficient

        weighted_sum_of_gammas = symbolics.add(weighted_sum_of_gammas)
        self.lp_model.add(
            Constraint(
                weighted_sum_of_gammas,
                lb=None,
                ub=sum_of_objective_coefficients - radius - 0.5,
                name=f"G@exclude_solution@{solution.solution_id:03d}",
            )
        )

    def write_lp(self, lp_path):
        """write the LP to this file (only for debugging)."""
        logging.debug(f"Writing LP to {lp_path}")
        with open(lp_path, "wt") as fp:
            json.dump(self.lp_model.to_json(), fp, indent="    ")

    def get_next_solution(self, counter: int) -> Union[Solution, None]:
        """Solve the LP and find the next shortest path.

        :return: A solution or None
        """
        logging.debug(f"Optimizing MILP")
        if self.lp_model.optimize() != "optimal":
            return None

        if "MDF_margin" in self.lp_model.variables:
            mdf = self.lp_model.variables["MDF_margin"].primal * self.comp_contrib.RT
        else:
            mdf = Q_("0 kJ/mol")

        var_df = pd.DataFrame(
            data=[
                (var_name, var.primal, var.lb, var.ub)
                for var_name, var in self.lp_model.variables.items()
            ],
            columns=["name", "primal", "lb", "ub"],
        )
        var_df["lp_type"] = "variable"

        cnstr_df = pd.DataFrame(
            data=[
                (cnstr_name, cnstr.primal, cnstr.lb, cnstr.ub)
                for cnstr_name, cnstr in self.lp_model.constraints.items()
            ],
            columns=["name", "primal", "lb", "ub"],
        )
        cnstr_df["lp_type"] = "constraint"

        return Solution(
            counter,
            self.cobra_model,
            self.cofactor_set,
            pd.concat([var_df, cnstr_df], axis=0),
            mdf,
        )

    def search(
        self,
        res_path: str,
        max_iterations: int = 1000,
        write_lp: bool = False,
        write_full_solution: bool = False,
        output_format: str = "escher",
        radius: int = 0,
    ) -> List[Tuple[float, float]]:
        """Find all the n-shortest paths that satisfy the constraints.
        :param res_path: the path in which to write the output files (prefix)
        :param max_iterations: maximum number of iterations
        :param write_lp: whether to write each of the LPs to files
        :param write_full_solution: write all variable and constraint primals to
        a file
        :param output_format: visual output format: "escher", "graphviz", "both" or
        "none"
        :param radius: the exclusion radius around older solutions
        :return: a list of (total flux, MDF) pairs for all solutions
        """
        solutions = []
        assert output_format in [
            "escher",
            "graphviz",
            "both",
            "none",
        ], f"Unknown output format: {output_format}"

        logging.debug("Creating the MILP for finding pathways")
        self.create_min_step_lp()

        sbtabdoc = SBtabDocument("pathways", filename=f"{res_path}.tsv")

        # Use iterative MILP to find the n-best optimal solutions
        for counter in range(max_iterations):
            if write_lp:
                self.write_lp(f"{res_path}_{counter:03d}.lp")

            solution = self.get_next_solution(counter)

            if solution is None:
                if counter == 0:
                    logging.info("Couldn't find any solutions")
                else:
                    logging.info(f"Couldn't find more than {counter} " f"solutions")
                return solutions

            logging.info(
                f"Found path #{counter:03d} comprising "
                f"{solution.n_reactions} reactions, "
                f"MDF = {solution.mdf:.2g}, "
                f"and a total flux = {solution.total_flux:.2g}"
            )
            solutions.append((solution.total_flux, solution.mdf))

            solution.write_to_sbtab(sbtabdoc)
            sbtabdoc.write()

            if write_full_solution:
                with open(f"{res_path}_{counter:03d}.csv", "w") as fp:
                    solution.df.to_csv(fp)

            if output_format in ["escher", "both"]:
                escher_builder = solution.to_escher()
                escher_builder.save_html(f"{res_path}_{counter:03d}.html")
            if output_format in ["graphviz", "both"]:
                g_dot, _, _ = solution.to_graph()
                g_dot.write_svg(f"{res_path}_{counter:03d}.svg", prog="dot")

            self.exclude_solution(solution, radius=radius)

        logging.info(
            f"Stopping after {max_iterations}, but more solutions " f"might exist"
        )

        return solutions
