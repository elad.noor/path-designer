# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import logging
import os
from typing import Dict, Iterable, List, Tuple, Union
from warnings import warn

import cobra
import pandas as pd
from equilibrator_api import ComponentContribution, Reaction
from equilibrator_cache import Compound

from . import REC_PATH


def cobra_met_to_equilibrator(
    met: Union[cobra.Metabolite, str], comp_contrib: ComponentContribution
) -> Compound:
    if type(met) == str:
        met_id = met
    else:
        met_id = met.id

    # TODO: this is extremely patchy, fix it in the source!
    if met_id == "micit_c":
        return comp_contrib.get_compound("kegg:C04593")

    try:
        # we drop the BiGG compartment suffix (e.g. "_c")
        return comp_contrib.get_compound(f"bigg.metabolite:{met_id[0:-2]}")
    except Exception as e:
        warn(f"Error when searching for reactant {met.id}", str(e))
        return None


def cobra_rxn_to_equilibrator(
    rxn: cobra.Reaction, comp_contrib: ComponentContribution
) -> Tuple[Union[Reaction, None], Dict[Compound, str]]:
    """Convert a cobra.Reaction to equilibrator_api.Reaction

    :param rxn: a cobra.Reaction
    :return: (reaction, compound_name_mapping)
    """
    compound_name_mapping = {}
    sparse = {}
    for met, coeff in rxn.metabolites.items():
        cpd = cobra_met_to_equilibrator(met, comp_contrib)
        if cpd is None:
            raise ValueError(
                f"cannot balance this reaction because the metabolite "
                f"{met.id} was not found in equilibrator-cache"
            )

        compound_name_mapping[cpd] = met.id
        sparse[cpd] = coeff
    eq_rxn = Reaction(sparse, rid=rxn.id)

    # if necessary, balance the reaction with water molecules (which are
    # commonly missing in BiGG models)
    balanced_eq_rxn = eq_rxn.balance_with_compound(
        comp_contrib.get_compound("bigg.metabolite:h2o"),
        ignore_atoms=("H",),
        raise_exception=False,
    )
    if balanced_eq_rxn is None:
        warn(f"reaction cannot be balanced: {rxn.build_reaction_string()}")
        return eq_rxn, compound_name_mapping
    else:
        return balanced_eq_rxn, compound_name_mapping


def bigg_id_to_equilibrator(
    cobra_model: cobra.Model,
    reaction_ids: Iterable[str],
    comp_contrib: ComponentContribution,
) -> Tuple[List[Reaction], Dict[Compound, str]]:
    """Convert a list of reactions to equilibrator_api.Reaction

    :param reaction_ids: a list of BiGG reaction IDs
    :return: (reactions, compound_name_mapping) where the former is a
    list of equilibrator_api.Reaction object, and the latter is a
    dictionary for mapping Compounds to the original BiGG IDs (useful for
    writing the reactions as text).
    """
    compound_name_mapping = {}
    reactions = []
    for reaction_id in reaction_ids:
        rxn = cobra_model.reactions.get_by_id(reaction_id)
        try:
            eq_rxn, mapping = cobra_rxn_to_equilibrator(rxn, comp_contrib)
            reactions.append(eq_rxn)
            compound_name_mapping.update(mapping)
        except ValueError as e:
            warn(f"Cannot convert reaction {reaction_id} to " f"eQuilibrator: {str(e)}")
            reactions.append(Reaction({}))
    return reactions, compound_name_mapping


def calculate_standard_dgs(
    cobra_model: cobra.Model, comp_contrib: ComponentContribution
) -> pd.DataFrame:
    """Calculate the standard dG' values fo an entire COBRA model."""
    p_h = comp_contrib.p_h.m_as("")
    i_s = comp_contrib.ionic_strength.m_as("M")
    thermo_fname = os.path.join(
        REC_PATH, f"thermo_{cobra_model.id}_ph{p_h:.2f}_I{i_s:.2f}.csv"
    )
    if os.path.exists(thermo_fname):
        logging.info(f"Found thermodynamics for {cobra_model.id}")
        standard_dg = pd.read_csv(thermo_fname, header=0, index_col=0)
    else:
        logging.info(
            f"Couldn't find a cache file {thermo_fname}. Calculating "
            f"thermodynamics for {cobra_model.id}"
        )
        standard_dg_data = []
        for cobra_reaction in cobra_model.reactions:
            if cobra_reaction.id[0:2] == "__":
                # skip manually added 'fake' reactions
                continue
            if cobra_reaction.compartments != {"c"}:
                # skip non-cytoplasmic reactions
                value = pd.np.nan
                error = pd.np.nan
                comment = "reaction is not purely in the cytoplasm"
            else:
                try:
                    eq_rxn, _ = cobra_rxn_to_equilibrator(cobra_reaction, comp_contrib)
                    std_dg_prime = comp_contrib.standard_dg_prime(eq_rxn)
                    std_dg_prime /= comp_contrib.RT  # convert to unitless
                    value = std_dg_prime.value.magnitude
                    error = std_dg_prime.error.magnitude
                    comment = ""
                except Exception as e:
                    value = pd.np.nan
                    error = pd.np.nan
                    comment = str(e)

            standard_dg_data.append((cobra_reaction.id, value, error, comment))

        standard_dg = pd.DataFrame(
            data=standard_dg_data,
            columns=[
                "bigg_id",
                "standard_dg_prime",
                "standard_dg_prime_error",
                "comment",
            ],
        )
        standard_dg.to_csv(thermo_fname)
        logging.info(f"Saving thermodynamics to {thermo_fname}")

    return standard_dg
