# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os

from equilibrator_api import Q_, ComponentContribution, Reaction
from equilibrator_cache import Compound
from equilibrator_pathway import Bounds, Pathway
from equilibrator_pathway.thermo_models import PathwayMDFData

relpath = lambda s: os.path.join(os.path.abspath(os.path.dirname(__file__)), f"../{s}")

REC_PATH = relpath("rec")
EXAMPLE_PATH = relpath("src/examples")
RES_PATH = relpath("res")

if not os.path.exists(RES_PATH):
    os.mkdir(RES_PATH)

from .mdf import MDF
from .path import Path
from .solution import Solution
