# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
from typing import Iterable, Tuple

import cobra.test
import numpy as np
import pandas as pd
import pydot
from sbtab import SBtab

from . import Q_, REC_PATH, Bounds, ComponentContribution, Pathway, PathwayMDFData
from .solution import Solution
from .util import bigg_id_to_equilibrator, calculate_standard_dgs


class MDF(object):
    PH = Q_("7.5")
    IONIC_STRENGTH = Q_("0.25M")
    TEMPERATURE = Q_("298.15 K")
    DEFAULT_COFACTOR_CONC_FNAME = os.path.join(REC_PATH, "cofactors.csv")
    COFACTORS = {
        "adp_c",
        "atp_c",
        "co2_c",
        "o2_c",
        "pi_c",
        "nh4_c",
        "glu__L_c",
        "akg_c",
        "h_c",
        "h_p",
        "h_e",
        "h2o_c",
        "nad_c",
        "nadh_c",
        "nadp_c",
        "nadph_c",
        "trdox_c",
        "trdrd_c",
        "q8_c",
        "q8h2_c",
        "mqn8_c",
        "mql8_c",
    }

    def __init__(
        self,
        cobra_model: cobra.Model,
        bounds_df: pd.DataFrame,
        comp_contrib: ComponentContribution,
    ):
        self.cobra_model = cobra_model
        self.bounds, self.name_to_compound = Bounds.from_dataframe(bounds_df)
        self.comp_contrib = comp_contrib
        self.cofactors = set()
        for met_id in MDF.COFACTORS:
            try:
                met = self.cobra_model.metabolites.get_by_id(met_id)
                self.cofactors.add(met)
            except KeyError:
                continue

        self.standard_dg = calculate_standard_dgs(self.cobra_model, self.comp_contrib)

    def analyze_sbtab(
        self, sbtab: SBtab.SBtabTable, comp_contrib: ComponentContribution
    ) -> PathwayMDFData:
        """Run MDF analysis on an SBtab input pathway

        :param sbtab: SBtabTable containing the pathway definition
        :param comp_contrib: a ComponentContribution object for calculating dGs
        :return:
        """
        df = sbtab.to_data_frame()
        df = df[df.type == "flux"]
        eq_reactions, name_mapping = bigg_id_to_equilibrator(
            self.cobra_model, df.reaction_id, self.comp_contrib
        )

        df["flux"] = df.primal.apply(float).to_numpy()
        df = df.join(self.standard_dg.set_index("bigg_id"), on="reaction_id")
        df.fillna(0, inplace=True)
        dg_sigma = np.zeros((df.shape[0], df.shape[0]))
        pp = Pathway(
            eq_reactions,
            fluxes=df.flux,
            standard_dg_primes=df.standard_dg_prime,
            dg_sigma=dg_sigma,
            bounds=self.bounds,
            p_h=comp_contrib.p_h,
            p_mg=comp_contrib.p_mg,
            ionic_strength=comp_contrib.ionic_strength,
            temperature=comp_contrib.temperature,
        )
        pp.set_compound_names(name_mapping.get)
        return pp.calc_mdf()

    def get_flux(self, mdf_data: PathwayMDFData) -> pd.DataFrame:
        reaction_df = mdf_data.reaction_df[["reaction_id", "flux"]].copy()
        reaction_df = reaction_df.rename(columns={"flux": "primal"})
        reaction_df["lp_type"] = "variable"
        reaction_df["name"] = "R@flux@" + reaction_df.reaction_id + "_F"
        return reaction_df.drop("reaction_id", axis=1)

    @staticmethod
    def get_bottleneck_reaction_ids(
        mdf_data: PathwayMDFData, threshold: float = 1e-5
    ) -> Iterable[str]:
        return mdf_data.reaction_df[
            mdf_data.reaction_df.shadow_price.abs() > threshold
        ].reaction_id.values

    @staticmethod
    def get_bottleneck_compound_ids(
        mdf_data: PathwayMDFData, threshold: float = 1e-5
    ) -> Iterable[str]:
        return mdf_data.compound_df[
            mdf_data.compound_df.shadow_price.abs() > threshold
        ].compound.values

    def draw_graph(self, mdf_data: PathwayMDFData) -> pydot.Dot:
        fluxes = self.get_flux(mdf_data)
        sol = Solution(self.cobra_model, self.cofactors, fluxes, mdf_data.mdf)
        g_dot, r_nodes, m_nodes = sol.to_graph()

        # highlight the bottleneck reactions (i.e. anything with a non-zero
        # shadow price)
        for rxn_id in MDF.get_bottleneck_reaction_ids(mdf_data):
            r_nodes[rxn_id].set("fillcolor", "red")

        return g_dot

    @staticmethod
    def beautify_tables(mdf_data: PathwayMDFData) -> Tuple[pd.DataFrame, pd.DataFrame]:
        """Change formatting of reaction and compound tables for HTML printout.

        :param mdf_data:
        :return: (reaction_df, compound_df)
        """
        reaction_df = mdf_data.reaction_df.loc[
            :,
            [
                "reaction_id",
                "physiological_dg_prime",
                "optimized_dg_prime",
                "shadow_price",
                "flux",
            ],
        ]
        reaction_df["flux"] = reaction_df.flux.round(3)
        reaction_df["&Delta;G'<sup>m</sup>"] = reaction_df[
            "physiological_dg_prime"
        ].apply(lambda x: "{0:.1f} kJ/mol".format(x.to("kJ/mol").magnitude))
        reaction_df["optimized &Delta;G'"] = reaction_df["optimized_dg_prime"].apply(
            lambda x: "{0:.1f} kJ/mol".format(x.to("kJ/mol").magnitude)
        )
        reaction_df["shadow price"] = reaction_df.shadow_price.round(1)
        reaction_df = reaction_df.drop(
            ["physiological_dg_prime", "optimized_dg_prime", "shadow_price"], axis=1
        )

        compound_df = mdf_data.compound_df
        compound_df["concentration"] = compound_df["concentration"].apply(
            lambda x: "{0:.3f} mM".format(x.to("mM").magnitude)
        )
        compound_df["lower_bound"] = compound_df["lower_bound"].apply(
            lambda x: "{0:.3f} mM".format(x.to("mM").magnitude)
        )
        compound_df["upper_bound"] = compound_df["upper_bound"].apply(
            lambda x: "{0:.3f} mM".format(x.to("mM").magnitude)
        )
        compound_df["shadow price"] = compound_df.shadow_price.round(1)
        compound_df = compound_df.drop(["shadow_price"], axis=1)

        return reaction_df, compound_df
